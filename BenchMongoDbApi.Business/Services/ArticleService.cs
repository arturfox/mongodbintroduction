﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BenchMongoDbApi.DataAccess.Repositories;
using BenchMongoDbApi.Entities;
using BenchMongoDbApi.ViewModels.Article;
using BenchMongoDbApi.ViewModels.Article.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace BenchMongoDbApi.Business.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly UserManager<AppUser> _userManager;

        public ArticleService(IArticleRepository articleRepository, 
            UserManager<AppUser> userManager)
        {
            _articleRepository = articleRepository;
            _userManager = userManager;
        }

        public async Task<bool> AddInfo(AddArticleModel model, string userId)
        {
            var article = new Article()
            {
                Body = model.Description,
                UserId = userId
            };
            await _articleRepository.Add(article);

            return true;
        }

        public async Task<List<ArticalGetArticalsListItem>> GetAllArticals()
        {
            var articals = await _articleRepository.GetAll();
            var users = _userManager.Users;

            var result = new List<ArticalGetArticalsListItem>();

            result.AddRange(articals.Join(users, aricle => aricle.UserId, user => user.Id, (aricle, user)
                => new ArticalGetArticalsListItem()
                {
                    Body = aricle.Body,
                    UserName = user.UserName
                })
            );

            return result;
        }
    }
}
