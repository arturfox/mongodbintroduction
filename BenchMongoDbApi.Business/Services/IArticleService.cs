﻿using BenchMongoDbApi.ViewModels.Article;
using BenchMongoDbApi.ViewModels.Article.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BenchMongoDbApi.Business.Services
{
    public interface IArticleService
    {
        Task<bool> AddInfo(AddArticleModel model,string userId);
        Task<List<ArticalGetArticalsListItem>> GetAllArticals();
    }
}
