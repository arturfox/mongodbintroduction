﻿using BenchMongoDbApi.Business.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BenchMongoDbApi.Business
{
  public class DependencyManager
    {
        public static void Configure(IServiceCollection services, string connectionString,string database)
        {
            DataAccess.DependencyManager.Configure(services, connectionString,database);
            services.AddTransient<IArticleService, ArticleService>();
        }
    }
}
