﻿namespace BenchMongoDbApi.Entities
{
    public class Article :BaseEntity
    {
        public string Body { get; set; } = string.Empty;

        public string UserId { get; set; }
    }
}
