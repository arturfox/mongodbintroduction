﻿namespace BenchMongoDbApi.ViewModels.Article.ViewModels
{
    public class ArticalGetArticalsListItem
    {
        public string Body { get; set; }
        public string UserName { get; set; }
    }
}
