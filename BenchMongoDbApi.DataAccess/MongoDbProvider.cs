﻿using BenchMongoDbApi.Entities;
using MongoDB.Driver;
using System.Linq;
using MongoDB.Bson;

namespace BenchMongoDbApi.DataAccess
{
    public class MongoDbProvider<TEntity> where TEntity:BaseEntity
    {
        private readonly IMongoDatabase _database;

        public MongoDbProvider(string connectionString, string dbName)
        {
            var client = new MongoClient(connectionString);
            if (client != null)
                _database = client.GetDatabase(dbName);

            var exist = CollectionExists();

            if (!exist)
            {
                CreateTable();
            }
        }

        public IMongoCollection<TEntity> Collection
        {
            get
            {
                return _database.GetCollection<TEntity>(typeof(TEntity).Name);
            }
        }

        private void CreateTable()
        {
            _database.CreateCollection(typeof(TEntity).Name);
        }

        public bool CollectionExists()
        {
            var filter = new BsonDocument("name", typeof(TEntity).Name);
            var options = new ListCollectionNamesOptions { Filter = filter };

            return _database.ListCollectionNames(options).Any();

        }
    }
}
