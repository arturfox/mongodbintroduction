﻿using BenchMongoDbApi.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BenchMongoDbApi.DataAccess.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        Task<List<TEntity>> GetAll();
        Task<TEntity> GetById(string Id);
        Task Add(TEntity AddingItem);
        Task AddRange(List<TEntity> listEntities);
        Task Update(TEntity UpdatingItem);
        Task UpdateRange(List<TEntity> listEntities);
        Task Delete(TEntity removingEntity);
    }
}
