﻿using BenchMongoDbApi.Entities;

namespace BenchMongoDbApi.DataAccess.Repositories
{
    public class ArticleRepository : BaseRepository<Article>, IArticleRepository
    {
        public ArticleRepository(DataConfiguration dataConfiguration):base(dataConfiguration)
        {

        }
    }
}
