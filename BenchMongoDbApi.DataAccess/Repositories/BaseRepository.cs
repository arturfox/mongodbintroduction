﻿using BenchMongoDbApi.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BenchMongoDbApi.DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly MongoDbProvider<TEntity> _provider;

        public BaseRepository(DataConfiguration dataConfiguration)
        {
            _provider = new MongoDbProvider<TEntity>
                (dataConfiguration.ConnectionString, dataConfiguration.Database);
        }

        public async Task<List<TEntity>> GetAll()
        {
            return await _provider.Collection
                .Find(_ => true).ToListAsync();
        }

        public async Task<TEntity> GetById(string id)
        {
            ObjectId internalId = GetInternalId(id);
            return await _provider.Collection
                            .Find(doc => doc.Id == id
                                    || doc.InternalId == internalId)
                            .FirstOrDefaultAsync();
        }

        public async Task Add(TEntity addingItem)
        {
            await _provider.Collection.InsertOneAsync(addingItem);
        }

        public async Task AddRange(List<TEntity> listEntities)
        {
            await _provider.Collection.InsertManyAsync(listEntities);

        }
        public async Task Update(TEntity updatingItem)
        {
            await _provider.Collection.ReplaceOneAsync(doc => doc.Id == updatingItem.Id, updatingItem);
        }

        public async Task UpdateRange(List<TEntity> listEntities)
        {
            foreach (TEntity document in listEntities)
            {
                await _provider.Collection.ReplaceOneAsync(doc => doc.Id == document.Id, document);
            }
        }

        public async Task Delete(TEntity removingItem)
        {
            await _provider.Collection.DeleteOneAsync(doc => doc.Id == removingItem.Id);
        }

        private ObjectId GetInternalId(string id)
        {
            if (!ObjectId.TryParse(id, out ObjectId internalId))
            {
                internalId = ObjectId.Empty;
            }

            return internalId;
        }
    }
}
