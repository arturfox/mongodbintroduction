﻿using BenchMongoDbApi.Entities;

namespace BenchMongoDbApi.DataAccess.Repositories
{
    public interface IArticleRepository:IBaseRepository<Article>
    {
    }
}
