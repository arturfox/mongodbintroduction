﻿namespace BenchMongoDbApi.DataAccess
{
    public class DataConfiguration
    {
        public string ConnectionString { get; }

        public string Database { get; }

        public DataConfiguration(string connectionString,string database)
        {
            ConnectionString = connectionString;
            Database = database;
        }
    }
}
