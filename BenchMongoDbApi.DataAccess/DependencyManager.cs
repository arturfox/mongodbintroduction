﻿using BenchMongoDbApi.DataAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace BenchMongoDbApi.DataAccess
{
    public class DependencyManager
    {
        public static void Configure(IServiceCollection services, string connectionString,string database)
        {
            services.AddSingleton(typeof(DataConfiguration), new DataConfiguration(connectionString,database));
            services.AddTransient<IArticleRepository, ArticleRepository>();
        }
    }
}
