﻿using BenchMongoDbApi.Entities;
using BenchMongoDbApi.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BenchMongoDbApi.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public AccountController(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");

            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password,true, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return Ok(new ResultBaseModel()
                    { isSuceed = true,
                    message="asdasd",
                    statusCode=200
                    }
                    );
                }
                return Ok(new ResultBaseModel()
                {
                    isSuceed = true,
                    message = "asdasd",
                    statusCode = 200
                });
            }

            return Ok(new ResultBaseModel()
            {
                isSuceed = true,
                message = "asdasd",
                statusCode = 200
            });
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new AppUser
                {
                    UserName = model.UserName,
                    Email = model.Email
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return Ok();
                }
                return Ok();
            }

            return BadRequest("MODEL IS NOT VALID");
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

    }

    public class ResultBaseModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public bool isSuceed { get; set; }
    }
}
