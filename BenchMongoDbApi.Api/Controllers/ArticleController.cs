﻿using System.Threading.Tasks;
using BenchMongoDbApi.Business.Services;
using BenchMongoDbApi.Entities;
using BenchMongoDbApi.ViewModels.Article;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BenchMongoDbApi.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleService _articleService;
        private readonly UserManager<AppUser> _userManager;

        public ArticleController(IArticleService articleService, 
            UserManager<AppUser> userManager)
        {
            _articleService = articleService;
            _userManager = userManager;
        }

        [HttpGet("all")]
        public async Task<ActionResult> GetAllArticles()
        {
            var articles = await _articleService.GetAllArticals();

            return Ok(articles);
        }

        [HttpPost("create")]
        public async Task<ActionResult> CreateArticle([FromBody] AddArticleModel model)
        {
            var user = await _userManager.GetUserAsync(User);

            await _articleService.AddInfo(model, user.Id);

            return Ok();
        }
    }
}